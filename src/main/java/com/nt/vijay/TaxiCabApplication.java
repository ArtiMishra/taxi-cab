package com.nt.vijay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaxiCabApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaxiCabApplication.class, args);
	}

}
